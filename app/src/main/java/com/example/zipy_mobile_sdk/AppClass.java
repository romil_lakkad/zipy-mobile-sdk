package com.example.zipy_mobile_sdk;

import android.app.Application;
import android.content.Context;

import com.example.zipy_mobile_sdk_module.ZipyModule;


public class AppClass extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        new ZipyModule(this);
        ZipyModule.setupAcra("vaibhav@prometteursolutions.com");
        ZipyModule.setupAnr();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        ZipyModule.startRecording(this);
    }
}
