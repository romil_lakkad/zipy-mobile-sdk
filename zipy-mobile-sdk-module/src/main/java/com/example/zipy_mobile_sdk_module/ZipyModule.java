package com.example.zipy_mobile_sdk_module;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.example.zipy_mobile_sdk_module.cofig.Config;
import com.example.zipy_mobile_sdk_module.receiver.ServiceRestarter;
import com.example.zipy_mobile_sdk_module.service.OnClearFromRecentService;
import com.example.zipy_mobile_sdk_module.service.ScreenRecoderService;
import com.github.anrwatchdog.ANRError;
import com.github.anrwatchdog.ANRWatchDog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.acra.ACRA;
import org.acra.BuildConfig;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.MailSenderConfigurationBuilder;
import org.acra.data.StringFormat;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ZipyModule extends Application {

    public static int mScreenWidth;
    public static int mScreenHeight;
    public static int mScreenDensity;
    private static final int REQUEST_CODE = 1000;

    public static CountDownTimer countDownTimer;

    public static Context context;

    public static ANRWatchDog anrWatchDog = new ANRWatchDog(2000);
    public static int duration = 4;

    public static ANRWatchDog.ANRListener silentListener = new ANRWatchDog.ANRListener() {
        @Override
        public void onAppNotResponding(@NonNull ANRError error) {
            Log.e("ANR-Watchdog-Demo", "", error);
        }
    };

    public ZipyModule(Context appClass) {
        context = appClass;
    }

    public static void startRecording(Context activity) {
        Intent intent = new Intent(activity, BaseActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public static void stopRecording() {
        if (recordingInterface != null) {
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            recordingInterface.onStopRecording();
        }
    }

    public static StopRecordingInterface recordingInterface;

    public static void startScreenRecoder(Activity activity, StopRecordingInterface recordingInterface1) {
        recordingInterface = recordingInterface1;
        Dexter.withActivity(activity).withPermissions(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
        ).withListener(new MultiplePermissionsListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    DisplayMetrics metrics = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
                    mScreenWidth = metrics.widthPixels;
                    mScreenHeight = metrics.heightPixels;
                    mScreenDensity = metrics.densityDpi;
                    startMediaProjectionApi(activity);
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    private static void startMediaProjectionApi(Activity activity) {
        MediaProjectionManager mediaProjectionManager = (MediaProjectionManager) activity.getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        Intent permissionIntent = mediaProjectionManager != null ? mediaProjectionManager.createScreenCaptureIntent() : null;
        activity.startActivityForResult(permissionIntent, REQUEST_CODE);
    }

    public static boolean getIsBusyRecording(Activity activity) {
        boolean isRecordingIsBusy = false;
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (ScreenRecoderService.class.getName().equals(service.service.getClassName())) {
                    isRecordingIsBusy = true;
                }
            }
        }
        return isRecordingIsBusy;
    }

    public static void stopScreenRecoder(Activity activity) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        Intent service = new Intent(activity, ScreenRecoderService.class);
        service.setAction(ScreenRecoderService.ACTION_STOP);
        activity.stopService(service);
    }

    public static void pauseRecoding(Activity activity) {
        Intent service = new Intent(activity, ScreenRecoderService.class);
        service.setAction(ScreenRecoderService.ACTION_PAUSE);
        activity.startService(service);
    }

    public static void resumeRecoding(Activity activity) {
        Intent service = new Intent(activity, ScreenRecoderService.class);
        service.setAction(ScreenRecoderService.ACTION_RESUME);
        activity.startService(service);
    }

    public static String getFileSize(long size) {
        String hrSize = null;

        double b = size;
        double k = size * 1024.0;
        double m = ((size * 1024.0) * 1024.0);

        DecimalFormat dec = new DecimalFormat("");
        hrSize = dec.format(m);
        return hrSize;
    }

    public static void onPause(Activity activity) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            activity.startService(new Intent(activity, OnClearFromRecentService.class));
            if (getIsBusyRecording(activity)) {
                pauseRecoding(activity);
            }
        } else {
            if (getIsBusyRecording(activity)) {
                stopScreenRecoder(activity);

                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("restartservice");
                broadcastIntent.setClass(activity, ServiceRestarter.class);
                activity.sendBroadcast(broadcastIntent);
            }
        }
    }

    public static void onDestroy(Activity activity) {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            activity.startService(new Intent(activity, OnClearFromRecentService.class));
            if (getIsBusyRecording(activity)) {
                pauseRecoding(activity);
            }
        } else {
            if (getIsBusyRecording(activity)) {
                stopScreenRecoder(activity);

                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("restartservice");
                broadcastIntent.setClass(activity, ServiceRestarter.class);
                activity.sendBroadcast(broadcastIntent);
            }
        }
    }

    public static void activityResult(int requestCode, int resultCode, Intent data, Activity activity) {
        if (requestCode == REQUEST_CODE) {
            Intent service = new Intent(activity, ScreenRecoderService.class);
            service.setAction(ScreenRecoderService.ACTION_START);
            service.putExtra("code", resultCode);
            service.putExtra("data", data);
            service.putExtra("width", mScreenWidth);
            service.putExtra("height", mScreenHeight);
            service.putExtra("density", mScreenDensity);
            if (StringUtils.isNotEmpty(Config.fileSize)) {
                service.putExtra("fileSize", getFileSize(Long.parseLong(Config.fileSize)));
            }
            if (StringUtils.isNotEmpty(Config.maxTimeExceed)) {
                timeForRecording(Integer.parseInt(Config.maxTimeExceed), activity);
            }
            activity.startService(service);
        } else {
            Toast.makeText(activity, "User Cancel the request", Toast.LENGTH_SHORT).show();
        }

    }

    public static void timeForRecording(int minits, Activity activity) {
        countDownTimer = new CountDownTimer(TimeUnit.MINUTES.toMillis(minits), 1000) {
            public void onTick(long millisUntilFinished) {
                NumberFormat f = new DecimalFormat("00");
                long sec = (millisUntilFinished / 1000) % 60;
                Log.e("SECOND=====>", sec + "");
            }

            // When the task is over it will print 00:00:00 there
            public void onFinish() {
                onTick(0);
                stopScreenRecoder(activity);

//                Intent broadcastIntent = new Intent();
//                broadcastIntent.setAction("restartservice");
//                broadcastIntent.setClass(activity, ServiceRestarter.class);
//                activity.sendBroadcast(broadcastIntent);

                startScreenRecoder(activity, recordingInterface);
            }
        }.start();
    }

    public static void setupAcra(String email) {
        CoreConfigurationBuilder builder1 = new CoreConfigurationBuilder(context);
        builder1.withBuildConfigClass(BuildConfig.class).withReportFormat(StringFormat.JSON);

        builder1.getPluginConfigurationBuilder(MailSenderConfigurationBuilder.class)
                .withMailTo(email)
                .withReportAsFile(true)
                .withReportFileName("Crash.txt")
                .withSubject("Crash Report Demo")
                .withBody("This Is Crash Report from Demo")
                .withEnabled(true);

//        builder1.getPluginConfigurationBuilder(ToastConfigurationBuilder.class)
//                .withResText(R.string.app_name)
//                .withEnabled(false)
//                //make sure to enable all plugins you want to use:
//                .withLength(Toast.LENGTH_LONG);

        ACRA.init((Application) context, builder1);
    }

    public static void setupAnr() {

        anrWatchDog.setANRListener(silentListener);

        anrWatchDog.setANRListener(new ANRWatchDog.ANRListener() {
            @Override
            public void onAppNotResponding(@NonNull ANRError error) {
                Log.e("ErrorMessages======>", error.getLocalizedMessage());
                // Some tools like ACRA are serializing the exception, so we must make sure the exception serializes correctly
                try {
                    new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(error);
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }

                Log.i("ANR-Watchdog-Demo", "Error was successfully serialized");
                throw error;
            }
        })
                .setANRInterceptor(new ANRWatchDog.ANRInterceptor() {
                    @Override
                    public long intercept(long duration) {
                        long ret = 0;
                        if (ret > 0)
                            Log.w("ANR-Watchdog-Demo", "Intercepted ANR that is too short (" + duration + " ms), postponing for " + ret + " ms.");
                        return ret;
                    }
                });

        anrWatchDog.start();

        anrWatchDog.setReportAllThreads();
        anrWatchDog.setANRListener(null);
    }
}
