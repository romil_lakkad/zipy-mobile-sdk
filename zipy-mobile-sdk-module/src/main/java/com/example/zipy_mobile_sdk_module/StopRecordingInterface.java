package com.example.zipy_mobile_sdk_module;

public interface StopRecordingInterface {
    public void onStopRecording();
}
